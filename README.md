# Phase One Pokemon

A simple test automation project to validate Pokemon data from several websites, created to complete Blibli.com FUTURE Batch 5 Phase 1 challenge.


## Prerequisites

In order to be able to use the project, you need to :

- have Java and IDE of your choice installed
- configure `~/.m2/settings.xml` as instructed in [Pokemon Reporting Plugin's usage guide](https://github.com/ubaifadhli/poke-reporting-plugin#usage-guide)


## Usage Guide

You can run the project by using `mvn clean verify` command.


## Technologies Used
- Cucumber
- Hamcrest
- Lombok
- RestAssured
- Selenium
- SLF4J
- TestNG
- WebDriverManager


## Challenge Progress
- [ **✓** ] UI & API automation
- [ **✓** ] Report file
- [ **✓** ] Automation data can be changed via properties
- [ **✓** ] Browser can be changed via properties
- [ **✓** ] UI screenshot in the report
- [ **✓** ] API request / response in the report
- [ **✓** ] Parallel mode
- [ **✓** ] Able to compare data from sources