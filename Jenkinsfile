pipeline {
  agent any

  environment {
        MAVEN_OPTS = '-Xmx512m'
        EMAIL_RECIPIENTS = 'muhammad.ubaidilah@gdn-commerce.com'
  }

  stages {

    stage('Compile project') {
      steps {
        sh 'mvn clean compile'
      }
    }

    stage('Scenario - Fetching data from each platform') {
      parallel {
        stage('API') {
          steps {
            sh 'mvn verify -Dcucumber.filter.tags=@API -Dcucumber.plugin=json:target/cucumber-report/api.json'
          }
        }

        stage('Mobile') {
          steps {
            sh 'mvn verify -Dcucumber.filter.tags=@Mobile -Dcucumber.plugin=json:target/cucumber-report/mobile.json'
          }
        }

        stage('Web') {
          steps {
            sh 'mvn verify -Dcucumber.filter.tags=@Web -Dcucumber.plugin=json:target/cucumber-report/web.json  -Dparallel.thread.count=2'
          }
        }
      }
    }

    stage('Scenario - Compare fetched data') {
      steps {
        sh 'mvn verify -Dcucumber.filter.tags=@Comparison -Dcucumber.plugin=json:target/cucumber-report/comparison.json'
      }
    }

    stage('Generate report') {
      steps {
        sh 'mvn com.github.ubaifadhli:poke-reporting-plugin:2.2.0:reporting'
      }
    }

  }

  post {
    always {
        emailext mimeType: 'text/html',
        			body: '${FILE, path="target/poke-report/email.html"}',
        			subject: 'Phase Two Pokemon - Report',
        			to: EMAIL_RECIPIENTS
    }
  }

  tools {
    maven 'Maven 3.8.4'
    jdk 'jdk8'
  }
}