@VerifyPokemonData
Feature: Verify Pokemon Data

  @API
  Scenario Outline: Fetch "<pokemon name>" Pokemon data from "<API name>" API
    When [API] user searches for "<pokemon name>" at "<API name>"
    Then [API] user should get success response
    And [API] user should be able to fetch necessary data

    Examples:
      | pokemon name               | API name |
      | {{ feature.pokemon.data }} | PokeAPI  |


  @Web
  Scenario Outline: Fetch "<pokemon name>" Pokemon data from "<website name>" website
    When [UI] user searches for "<pokemon name>" at "<website name>"
    Then [UI] user should be at detail page of specified pokemon
    And [UI] user should be able to fetch necessary data

    Examples:
      | pokemon name               | website name |
      | {{ feature.pokemon.data }} | Bulbapedia   |
      | {{ feature.pokemon.data }} | PokemonDB    |


  @Mobile
  Scenario Outline: Fetch "<pokemon name>" Pokemon data from "<mobile application name>" mobile application
    When [UI] user searches for "<pokemon name>" at "<mobile application name>"
    And [UI] user should be able to fetch necessary data

    Examples:
      | pokemon name               | mobile application name |
      | {{ feature.pokemon.data }} | Pokedex                 |


  @Comparison
  Scenario: Compare fetched Pokemon data from each platform
    When User compares fetched Pokemon data from each application
    Then User should see the same value of data across every website
      | {{ feature.pokemon.data }} |