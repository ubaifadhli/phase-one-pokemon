package com.ubaifadhli.future;

import com.ubaifadhli.future.util.DriverFactory;
import com.ubaifadhli.future.util.PropertiesReader;
import com.ubaifadhli.future.util.SharedState;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class Hooks {
    private static final String WEB_DRIVER_CONFIG_PROPERTIES = "driver-config-web.properties";
    private static final String MOBILE_DRIVER_CONFIG_PROPERTIES = "driver-config-mobile.properties";
    private static final String REMOTE_ENABLED_PROPERTY = "remote.enabled";

    private final SharedState sharedState;

    public Hooks(SharedState sharedState) {
        this.sharedState = sharedState;
    }

    @Before("@Web")
    public void beforeScenarioWeb() {
        PropertiesReader reader = new PropertiesReader(WEB_DRIVER_CONFIG_PROPERTIES);

        boolean isRemote = Boolean.parseBoolean(reader.getProperty(REMOTE_ENABLED_PROPERTY));

        sharedState.driver = isRemote ? DriverFactory.createRemoteWebDriver(WEB_DRIVER_CONFIG_PROPERTIES) : DriverFactory.createLocalDriver();

        sharedState.driver.manage().window().maximize();
    }

    @Before("@Mobile")
    public void beforeScenarioMobile() {
        sharedState.driver = DriverFactory.createRemoteMobileDriver(MOBILE_DRIVER_CONFIG_PROPERTIES);
    }

    @After("@Web or @Mobile")
    public void afterScenarioUI() {
        if (doesBrowserExist())
            destroyBrowser();
    }

    @AfterStep("@Web or @Mobile")
    public void afterStepUI(Scenario scenario) {
        scenario.attach(getScreenshotAsBytes(), "image/png", "");
        if (scenario.isFailed())
            destroyBrowser();
    }

    @AfterStep("@API or @Comparison")
    public void afterStepAPI(Scenario scenario) {
        if (sharedState.logData != null) {
            scenario.attach(sharedState.logData, "text/plain", "Log data");
            sharedState.logData = null;
        }
    }

    public void destroyBrowser() {
        sharedState.driver.quit();
        sharedState.driver = null;
    }

    public boolean doesBrowserExist() {
        return sharedState.driver != null;
    }

    public byte[] getScreenshotAsBytes() {
        TakesScreenshot screenshot = (TakesScreenshot) sharedState.driver;
        return screenshot.getScreenshotAs(OutputType.BYTES);
    }
}
