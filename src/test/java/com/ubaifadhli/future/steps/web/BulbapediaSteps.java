package com.ubaifadhli.future.steps.web;

import com.ubaifadhli.future.data.PokemonData;
import com.ubaifadhli.future.page.bulbapedia.BulbapediaDetailPage;
import com.ubaifadhli.future.page.bulbapedia.BulbapediaHomePage;
import com.ubaifadhli.future.steps.PokemonWebSteps;
import com.ubaifadhli.future.util.SharedState;
import com.ubaifadhli.future.util.StringFormattingHelper;
import com.ubaifadhli.future.constant.ApplicationConstant;

public class BulbapediaSteps implements PokemonWebSteps {
    private SharedState sharedState;
    private BulbapediaHomePage homePage;
    private BulbapediaDetailPage detailPage;
    private String pokemonName;

    public BulbapediaSteps(SharedState sharedState) {
        this.sharedState = sharedState;
        initializePageObject();
    }

    @Override
    public void initializePageObject() {
        homePage = new BulbapediaHomePage(sharedState.driver);
        detailPage = new BulbapediaDetailPage(sharedState.driver);
    }

    @Override
    public String getWebsiteUrl() {
        return ApplicationConstant.Bulbapedia.url;
    }

    @Override
    public String getCurrentUrl() {
        return detailPage.getCurrentUrl();
    }

    @Override
    public void searchForPokemon(String pokemonName) {
        this.pokemonName = pokemonName;

        homePage.openPageAt(getWebsiteUrl());
        homePage.typeAndClickSearchInput(this.pokemonName);

        // Workaround for Firefox problem of not waiting the page to fully load.
        homePage.waitForWelcomeTextToStale();
    }

    @Override
    public String getExpectedDetailPageUrl() {
        return getWebsiteUrl() + StringFormattingHelper.uppercaseFirstCharacter(pokemonName);
    }

    @Override
    public PokemonData fetchPokemonData() {
        PokemonData pokemonData = PokemonData.builder()
                .name(pokemonName)
                .types(detailPage.getPokemonType(pokemonName))
                .number(detailPage.getPokemonNumber())
                .build();

        pokemonData.setStatData(detailPage.getPokemonBaseStats());

        return pokemonData;
    }
}
