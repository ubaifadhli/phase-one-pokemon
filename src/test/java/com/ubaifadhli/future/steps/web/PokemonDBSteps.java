package com.ubaifadhli.future.steps.web;

import com.ubaifadhli.future.data.PokemonData;
import com.ubaifadhli.future.page.pokemondb.PokemonDBDetailPage;
import com.ubaifadhli.future.page.pokemondb.PokemonDBHomePage;
import com.ubaifadhli.future.page.pokemondb.PokemonDBSearchPage;
import com.ubaifadhli.future.steps.PokemonWebSteps;
import com.ubaifadhli.future.exception.PokemonNotFoundException;
import com.ubaifadhli.future.util.SharedState;
import com.ubaifadhli.future.constant.ApplicationConstant;
import org.openqa.selenium.Keys;

public class PokemonDBSteps implements PokemonWebSteps {
    private SharedState sharedState;

    private PokemonDBHomePage homePage;
    private PokemonDBDetailPage detailPage;
    private PokemonDBSearchPage searchPage;

    private String pokemonName;

    public PokemonDBSteps(SharedState sharedState) {
        this.sharedState = sharedState;
        initializePageObject();
    }

    @Override
    public void initializePageObject() {
        homePage = new PokemonDBHomePage(sharedState.driver);
        detailPage = new PokemonDBDetailPage(sharedState.driver);
        searchPage = new PokemonDBSearchPage(sharedState.driver);
    }

    @Override
    public String getWebsiteUrl() {
        return ApplicationConstant.PokemonDB.url;
    }

    @Override
    public String getCurrentUrl() {
        return detailPage.getCurrentUrl();
    }

    @Override
    public void searchForPokemon(String pokemonName) {
        this.pokemonName = pokemonName;

        homePage.openPageAt(getWebsiteUrl());

        if (homePage.isPrivacyModalVisible())
            homePage.clickCloseModalButton();

        // Clicking the modal button twice to prevent inconsistent closing of the modal.
        if (homePage.isPrivacyModalVisible())
            homePage.clickCloseModalButton();

        homePage.typeSearchInput(pokemonName, Keys.ENTER);

        // Workaround for Firefox problem of not waiting the page to fully load.
        homePage.waitForWelcomeTextToStale();

        // Clicking the modal button thrice to prevent inconsistent closing of the modal.
        if (searchPage.isPrivacyModalVisible())
            searchPage.clickCloseModalButton();

        if (!searchPage.doesNoResultBoxExists() && searchPage.getFirstResultBoldText().equalsIgnoreCase(pokemonName))
            searchPage.clickFirstResultLink();
        else
            throw new PokemonNotFoundException(pokemonName);
    }

    @Override
    public String getExpectedDetailPageUrl() {
        return getWebsiteUrl() + "pokedex/" + pokemonName;
    }

    @Override
    public PokemonData fetchPokemonData() {
        PokemonData pokemonData = PokemonData.builder()
                .name(pokemonName)
                .types(detailPage.getPokemonTypes())
                .number(detailPage.getPokemonNumber())
                .build();

        pokemonData.setStatData(detailPage.getPokemonBaseStat());

        return pokemonData;
    }
}
