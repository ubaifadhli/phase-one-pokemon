package com.ubaifadhli.future.steps;

import com.ubaifadhli.future.data.PokemonData;

public interface PokemonUISteps {
    void initializePageObject();

    void searchForPokemon(String pokemonName);

    PokemonData fetchPokemonData();
}
