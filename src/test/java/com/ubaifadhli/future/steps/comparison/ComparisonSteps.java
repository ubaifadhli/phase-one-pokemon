package com.ubaifadhli.future.steps.comparison;

import com.ubaifadhli.future.constant.ApplicationConstant;
import com.ubaifadhli.future.data.PokemonData;
import com.ubaifadhli.future.exception.UnexpectedPokemonException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ComparisonSteps {
    public Map<ApplicationConstant, PokemonData> getPokemonDataFromEachMapEntry(Map<ApplicationConstant, List<PokemonData>> pokemonDataMap, String pokemonName) {
        int UNKNOWN = -1;
        int NOT_FOUND = 0;
        int FOUND = 1;

        int searchResult = UNKNOWN;

        Map<ApplicationConstant, PokemonData> foundPokemonMap = new HashMap<>();

        // Checking if specified pokemon can be found in each application data.
        for (Map.Entry<ApplicationConstant, List<PokemonData>> pokemonDataSet : pokemonDataMap.entrySet()) {
            PokemonData foundPokemon = pokemonDataSet.getValue()
                    .stream()
                    .filter(data -> data.getName().equals(pokemonName))
                    .findFirst()
                    .orElse(null);

            boolean isPokemonFound = foundPokemon != null;

            if (searchResult == UNKNOWN)
                searchResult = isPokemonFound ? FOUND : NOT_FOUND;

                // Expects the data consistently found / not found in each application.
            else if (searchResult != (isPokemonFound ? FOUND : NOT_FOUND))
                throw new UnexpectedPokemonException(pokemonName, isPokemonFound, pokemonDataSet.getKey());

            if (isPokemonFound)
                foundPokemonMap.put(pokemonDataSet.getKey(), foundPokemon);
        }

        return searchResult == FOUND ? foundPokemonMap : null;
    }
}
