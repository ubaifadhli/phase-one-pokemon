package com.ubaifadhli.future.steps;

public interface PokemonWebSteps extends PokemonUISteps {
    String getWebsiteUrl();

    String getCurrentUrl();

    String getExpectedDetailPageUrl();
}
