package com.ubaifadhli.future.steps.mobile;

import com.ubaifadhli.future.constant.PokedexStatNameConstant;
import com.ubaifadhli.future.data.PokemonData;
import com.ubaifadhli.future.page.pokedex.PokedexPage;
import com.ubaifadhli.future.steps.PokemonUISteps;
import com.ubaifadhli.future.exception.PokemonNotFoundException;
import com.ubaifadhli.future.util.SharedState;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PokedexSteps implements PokemonUISteps {
    private SharedState sharedState;
    private PokedexPage pokedexPage;

    public PokedexSteps(SharedState sharedState) {
        this.sharedState = sharedState;
        initializePageObject();
    }

    @Override
    public void initializePageObject() {
        pokedexPage = new PokedexPage(sharedState.driver);
    }

    @Override
    public void searchForPokemon(String pokemonName) {
        pokedexPage.clickPopupContinueButton();

        pokedexPage.clickAndTypeSearchView(pokemonName);

        pokedexPage.clickEnterOnSearchView();

        int foundIndex = pokedexPage.getSearchedPokemonIndex(pokemonName);

        if (foundIndex >= 0)
            pokedexPage.clickPokemonSearchResultByIndex(foundIndex);
        else
            throw new PokemonNotFoundException(pokemonName);
    }

    @Override
    public PokemonData fetchPokemonData() {
        PokemonData pokemonData = PokemonData.builder()
                .name(pokedexPage.getPokemonName().toLowerCase())
                .number(pokedexPage.getPokemonNumber())
                .build();

        pokedexPage.scrollUp();

        List<Integer> statNumbers = Arrays.stream(PokedexStatNameConstant.values())
                .map(statName -> pokedexPage.getTextFromClickableElement(pokedexPage.getPokemonStatNumberByName(statName)))
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        int totalStatNumber = statNumbers.stream().mapToInt(Integer::intValue).sum();
        statNumbers.add(totalStatNumber);

        pokemonData.setStatData(statNumbers);

        return pokemonData;
    }
}
