package com.ubaifadhli.future.steps;

import com.github.ubaifadhli.core.CSVProcessor;
import com.shazam.shazamcrest.matcher.Matchers;
import com.ubaifadhli.future.constant.ApplicationConstant;
import com.ubaifadhli.future.controller.PokeAPIController;
import com.ubaifadhli.future.controller.PokemonAPIController;
import com.ubaifadhli.future.data.APIResult;
import com.ubaifadhli.future.data.PokemonData;
import com.ubaifadhli.future.data.pokeapi.response.PokeAPIResponse;
import com.ubaifadhli.future.steps.comparison.ComparisonSteps;
import com.ubaifadhli.future.steps.mobile.PokedexSteps;
import com.ubaifadhli.future.steps.web.BulbapediaSteps;
import com.ubaifadhli.future.steps.web.PokemonDBSteps;
import com.ubaifadhli.future.util.PropertiesReader;
import com.ubaifadhli.future.util.SharedState;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class VerifyPokemonDataSteps {
    private final String APPLICATION_PROPERTIES = "application.properties";
    private final String POKEMON_DATA_DIRECTORY_PROPERTIES = "pokemon.data.directory";

    private final SharedState sharedState;

    private PokemonUISteps pokemonUISteps;
    private ComparisonSteps comparisonSteps;
    private PokemonAPIController pokemonAPIController;

    private ApplicationConstant currentApplicationConstant;

    private String pokemonName;
    private Map<ApplicationConstant, List<PokemonData>> pokemonDataMap;

    private PropertiesReader reader;
    private Response apiResponse;

    public VerifyPokemonDataSteps(SharedState sharedState) {
        this.sharedState = sharedState;
        pokemonDataMap = new HashMap<>();

        reader = new PropertiesReader(APPLICATION_PROPERTIES);
    }

    @When("[UI] user searches for {string} at {string}")
    public void uiUserSearchesForAtWebsite(String pokemonName, String pokemonWebsite) {
        this.pokemonName = pokemonName;

        currentApplicationConstant = ApplicationConstant.valueOf(pokemonWebsite);

        switch (currentApplicationConstant) {
            case Bulbapedia:
                pokemonUISteps = new BulbapediaSteps(sharedState);
                break;

            case PokemonDB:
                pokemonUISteps = new PokemonDBSteps(sharedState);
                break;

            case Pokedex:
                pokemonUISteps = new PokedexSteps(sharedState);
                break;
        }

        pokemonUISteps.searchForPokemon(this.pokemonName);
    }

    @Then("[UI] user should be at detail page of specified pokemon")
    public void uiUserShouldBeAtDetailPageOfSpecifiedPokemon() {
        PokemonWebSteps currentSteps = ((PokemonWebSteps) pokemonUISteps);

        // TODO conditional to check if current steps is instance of PokemonWebSteps or not

        assertThat(
                "User is not in detail page.",
                currentSteps.getCurrentUrl(),
                containsString(currentSteps.getExpectedDetailPageUrl())
        );
    }

    @And("[UI] user should be able to fetch necessary data")
    public void uiUserShouldBeAbleToFetchNecessaryData() {
        PokemonData pokemonData = pokemonUISteps.fetchPokemonData();

        List<PokemonData> tempPokemonDataList = new ArrayList<>();
        tempPokemonDataList.add(pokemonData);

        String currentFilePath = reader.getProperty(POKEMON_DATA_DIRECTORY_PROPERTIES) + currentApplicationConstant.name().toLowerCase() + ".csv";

        CSVProcessor<PokemonData> csvProcessor = new CSVProcessor<>(currentFilePath, PokemonData.class);

        csvProcessor.writeToFile(tempPokemonDataList);
    }

    @When("[API] user searches for {string} at {string}")
    public void apiUserSearchesForAtWebsite(String pokemonName, String pokemonWebsite) {
        currentApplicationConstant = ApplicationConstant.valueOf(pokemonWebsite);

        switch (currentApplicationConstant) {
            case PokeAPI:
                pokemonAPIController = new PokeAPIController();
                break;
        }

        APIResult apiResult = pokemonAPIController.getPokemonData(pokemonName);

        apiResponse = apiResult.getResponse();
        sharedState.logData = apiResult.getRequestLog();
    }

    @Then("[API] user should get success response")
    public void apiUserShouldGetSuccessResponse() {
        assertThat(
                "Failed to request the pokemon data.",
                apiResponse.getStatusCode(),
                equalTo(200)
        );

        sharedState.logData = apiResponse.asPrettyString();
    }

    @Then("[API] user should be able to fetch necessary data")
    public void apiUserShouldBeAbleToFetchNecessaryData() {
        PokeAPIResponse pokeAPIResponse = apiResponse.as(PokeAPIResponse.class);
        PokemonData pokemonData = pokeAPIResponse.toPokemonData();

        List<PokemonData> tempPokemonDataList = new ArrayList<>();
        tempPokemonDataList.add(pokemonData);

        String currentFilePath = reader.getProperty(POKEMON_DATA_DIRECTORY_PROPERTIES) + currentApplicationConstant.name().toLowerCase() + ".csv";

        CSVProcessor<PokemonData> csvProcessor = new CSVProcessor<>(currentFilePath, PokemonData.class);

        csvProcessor.writeToFile(tempPokemonDataList);
    }

    @Then("User should see the same value of data across every website")
    public void userShouldSeeTheSameValueOfDataAcrossEveryWebsite(DataTable pokemonDataTable) {
        List<String> pokemonNames = pokemonDataTable.asList();

        StringBuilder logDataStringBuilder = new StringBuilder();

        for (String pokemonName : pokemonNames) {
            Map<ApplicationConstant, PokemonData> foundPokemonMap = comparisonSteps.getPokemonDataFromEachMapEntry(pokemonDataMap, pokemonName);
            boolean isPokemonFound = foundPokemonMap != null;

            // For logging purpose.
            logDataStringBuilder
                    .append(pokemonName)
                    .append("\n");

            if (isPokemonFound)
                foundPokemonMap.forEach(((applicationConstant, pokemonData) ->
                        logDataStringBuilder
                                .append(applicationConstant)
                                .append(" | ")
                                .append(pokemonData)
                                .append("\n")
                ));

            logDataStringBuilder
                    .append("\n")
                    .append("\n");

            sharedState.logData = logDataStringBuilder.toString();

            // If pokemon data are found in each platform then comparison would be performed.
            if (isPokemonFound) {
                List<ApplicationConstant> constantList = Arrays.asList(ApplicationConstant.values());

                for (int i = 1; i < constantList.size(); i++) {
                    ApplicationConstant firstConstant = constantList.get(0);
                    ApplicationConstant secondConstant = constantList.get(i);

                    boolean isMobile = firstConstant.equals(ApplicationConstant.Pokedex) || secondConstant.equals(ApplicationConstant.Pokedex);

                    assertThat(
                            foundPokemonMap.get(firstConstant),
                            isMobile ? // Ignoring types field when asserting mobile.
                                    Matchers.sameBeanAs(foundPokemonMap.get(secondConstant)).ignoring("types") :
                                    Matchers.sameBeanAs(foundPokemonMap.get(secondConstant))
                    );
                }
            }
        }
    }

    @When("User compares fetched Pokemon data from each application")
    public void userComparesFetchedPokemonDataFromEachApplication() {
        comparisonSteps = new ComparisonSteps();

        Arrays.stream(ApplicationConstant.values())
                .forEach(application -> {
                    String applicationFilename = reader.getProperty(POKEMON_DATA_DIRECTORY_PROPERTIES) + application.name().toLowerCase() + ".csv";

                    CSVProcessor<PokemonData> csvProcessor = new CSVProcessor<>(applicationFilename, PokemonData.class);
                    pokemonDataMap.put(application, csvProcessor.readFile());
                });
    }
}
