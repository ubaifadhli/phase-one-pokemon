package com.ubaifadhli.future.util;

public class StringFormattingHelper {
    public static String uppercaseFirstCharacter(String word) {
        return word.substring(0, 1).toUpperCase() +
                word.substring(1);
    }
}
