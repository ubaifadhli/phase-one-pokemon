package com.ubaifadhli.future.util;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DriverFactory {
    private static final String APPLICATION_PROPERTIES = "application.properties";
    private static final String WEB_DRIVER_CONFIG_PROPERTIES = "driver-config-web.properties";

    private static final String BROWSER_TYPE_PROPERTIES = "browser.type";
    private static final String BASE_BROWSER_EXTENSION = "browser.extension";
    private static final String CHROMIUM_BROWSER_EXTENSION = BASE_BROWSER_EXTENSION + ".chromium";
    private static final String FIREFOX_BROWSER_EXTENSION = BASE_BROWSER_EXTENSION + ".firefox";

    private static final String BASE_EXTENSION_PATH = "src/test/resources/extensions/";

    private static final String BASE_DESIRED_CAPABILITIES_PROPERTY = "remote.desiredcapabilities.";

    private static final String REMOTE_URL = "remote.url";

    private static void loadDriver(DriverManagerType driverType) {
        WebDriverManager.getInstance(driverType).setup();
    }

    public static WebDriver createLocalDriver() {
        PropertiesReader reader = new PropertiesReader(APPLICATION_PROPERTIES);

        String chosenBrowser = reader.getProperty(BROWSER_TYPE_PROPERTIES);
        DriverManagerType driverType = DriverManagerType.valueOf(chosenBrowser);
        loadDriver(driverType);

        WebDriver webDriver;

        switch (driverType) {
            case FIREFOX:
                FirefoxDriver firefoxDriver = new FirefoxDriver();

                String browserName = DriverManagerType.FIREFOX.getBrowserName().toLowerCase();

                if (isExtensionEnabled(browserName))
                    firefoxDriver.installExtension(getExtensionAsFile(browserName).toPath());

                webDriver = firefoxDriver;

                break;

            default:
                ChromeOptions chromeOptions = new ChromeOptions();

                browserName = DriverManagerType.CHROME.getBrowserName().toLowerCase();

                if (isExtensionEnabled(browserName))
                    chromeOptions.addExtensions(getExtensionAsFile(browserName));

                webDriver = new ChromeDriver(chromeOptions);

                break;
        }

        return webDriver;
    }

    public static boolean isExtensionEnabled(String browserName) {
        PropertiesReader reader = new PropertiesReader(WEB_DRIVER_CONFIG_PROPERTIES);

        String extensionRelativePath = reader.getProperty(
                browserName.equals("firefox") ?
                        FIREFOX_BROWSER_EXTENSION :
                        CHROMIUM_BROWSER_EXTENSION
        );

        return extensionRelativePath != null && extensionRelativePath.length() > 0;
    }

    public static DesiredCapabilities getDesiredCapabilities(String propertiesFilename) {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        PropertiesReader reader = new PropertiesReader(propertiesFilename);

        List<String> propertyKeys = reader.getPropertyKeys()
                .stream()
                .filter(key -> key.contains(BASE_DESIRED_CAPABILITIES_PROPERTY))
                .collect(Collectors.toList());

        propertyKeys.forEach(propertyKey -> {
            String capabilityValue = reader.getProperty(propertyKey);
            String capabilityKey = propertyKey.replace(BASE_DESIRED_CAPABILITIES_PROPERTY, "");

            desiredCapabilities.setCapability(capabilityKey, capabilityValue);
        });

        return desiredCapabilities;
    }

    public static RemoteWebDriver createRemoteWebDriver(String propertiesFilename) {
        PropertiesReader reader = new PropertiesReader(propertiesFilename);
        DesiredCapabilities desiredCapabilities = getDesiredCapabilities(propertiesFilename);

        String browserName = desiredCapabilities.getBrowserName();

        getExtensionCapabilityByRemoteBrowser(browserName)
                .forEach(desiredCapabilities::setCapability);

        try {
            URL remoteDriverURL = URI.create(reader.getProperty(REMOTE_URL)).toURL();
            return new RemoteWebDriver(remoteDriverURL, desiredCapabilities);

        } catch (MalformedURLException e) {
            throw new RuntimeException("Provided URL is not valid. Please provide a valid URL.");
        }
    }

    public static Map<String, Object> getExtensionCapabilityByRemoteBrowser(String remoteBrowserName) {
        Map<String, Object> capabilityMap = new HashMap<>();

        if (remoteBrowserName.equals("chrome") && isExtensionEnabled(remoteBrowserName))
            capabilityMap.put(ChromeOptions.CAPABILITY, new ChromeOptions().addExtensions(getExtensionAsFile("chrome")));

        else {
            FirefoxProfile firefoxProfile = new FirefoxProfile();
            firefoxProfile.addExtension(getExtensionAsFile("firefox"));

            capabilityMap.put(FirefoxDriver.PROFILE, firefoxProfile);
        }

        return capabilityMap;
    }

    public static File getExtensionAsFile(String browserName) {
        PropertiesReader reader = new PropertiesReader(WEB_DRIVER_CONFIG_PROPERTIES);

        String extensionRelativePath = reader.getProperty(
                browserName.equals("firefox") ?
                        FIREFOX_BROWSER_EXTENSION :
                        CHROMIUM_BROWSER_EXTENSION
        );

        return new File(BASE_EXTENSION_PATH + extensionRelativePath);
    }

    public static AppiumDriver createRemoteMobileDriver(String propertiesFilename) {
        PropertiesReader reader = new PropertiesReader(propertiesFilename);
        DesiredCapabilities desiredCapabilities = getDesiredCapabilities(propertiesFilename);

        try {
            URL remoteDriverURL = URI.create(reader.getProperty(REMOTE_URL)).toURL();

            return new AndroidDriver<AndroidElement>(remoteDriverURL, desiredCapabilities);

        } catch (MalformedURLException e) {
            throw new RuntimeException("Provided URL is not valid. Please provide a valid URL.");
        }
    }
}
