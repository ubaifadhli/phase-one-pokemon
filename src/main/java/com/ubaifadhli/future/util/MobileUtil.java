package com.ubaifadhli.future.util;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.WebDriver;

import java.time.Duration;

public class MobileUtil {
    public static AndroidDriver getAsAndroidDriver(WebDriver driver) {
        return (AndroidDriver) driver;
    }

    public static void pressEnter(WebDriver driver) {
        getAsAndroidDriver(driver).pressKey(new KeyEvent(AndroidKey.ENTER));
    }

    public static void scrollUp(WebDriver driver, int scrollDownPercentage) {
        int deviceMiddleY = getAsAndroidDriver(driver).manage().window().getSize().getHeight() / 2;
        int deviceMiddleX = getAsAndroidDriver(driver).manage().window().getSize().getWidth() / 2;

        if (scrollDownPercentage < 0 || scrollDownPercentage > 100)
            throw new RuntimeException("ScrollDownPercentage should be ranged from 0 to 100.");

        // The lowest value for this coordinate is 0, which means that it will scroll up to the top of the screen.
        int deviceEndScrollY = (int) ( deviceMiddleY * (double) (100 - scrollDownPercentage) / 100);

        TouchAction touchAction = new TouchAction(getAsAndroidDriver(driver));

        touchAction.press(PointOption.point(deviceMiddleX, deviceMiddleY))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1))) // IDK why but it wont work without this.
                .moveTo(PointOption.point(deviceMiddleX, deviceEndScrollY))
                .release()
                .perform();
    }


}
