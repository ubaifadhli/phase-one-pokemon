package com.ubaifadhli.future.util;

import lombok.extern.java.Log;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Log
public class PropertiesReader {
    private Properties properties;
    private InputStream inputStream;

    public PropertiesReader(String propertiesFileName) {
        properties = new Properties();
        inputStream = getClassLoader().getResourceAsStream(propertiesFileName);

        try {
            properties.load(inputStream);

        } catch (IOException e) {
            log.warning("Cannot load properties with specified name :" + propertiesFileName);
            e.printStackTrace();
        }
    }

    public ClassLoader getClassLoader() {
        return PropertiesReader.class.getClassLoader();
    }

    public List<String> getPropertyKeys() {
        return new ArrayList<>(properties.stringPropertyNames());
    }

    public String getProperty(String propertyName) {
        return properties.getProperty(propertyName);
    }

    // TODO Possible unused method
    public File getFileFromProperty(String propertyName) {
        String propertyValue = getProperty(propertyName);

        URL filePath = getClassLoader().getResource(propertyValue);

        try {
            return new File(filePath.toURI());

        } catch (URISyntaxException uriSyntaxException) {
            throw new RuntimeException("Failed to get file of property name : " + propertyName);
        }
    }
}
