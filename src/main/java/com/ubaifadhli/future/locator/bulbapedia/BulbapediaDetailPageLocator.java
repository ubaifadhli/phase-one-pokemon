package com.ubaifadhli.future.locator.bulbapedia;

public interface BulbapediaDetailPageLocator {
    // Value : Pokemon name
    String POKEMON_FIRST_FORM_TYPE_TEXTS = "//table[following-sibling::small[text()='%s']]//b[ancestor::a[not(contains(@class, 'redirect'))]]";

    String POKEMON_STAT_TABLES = "//table[following-sibling::h4[child::span[text()='Pokéathlon stats']] and preceding-sibling::h3[child::span[@id='Stats']]]";
    String POKEMON_STAT_TABLE_SPANS = "//h5[following-sibling::h4[child::span[text()='Pokéathlon stats']] and preceding-sibling::h3[child::span[@id='Stats']]]/span";

    // Value : Table index
    String POKEMON_STAT_VALUES_BY_TABLE_INDEX = POKEMON_STAT_TABLES + "[%d]//div[@style='float:right']";

    String POKEMON_NUMBER_SPAN = "//span[contains(text(), '#') and parent::a[contains(@title, 'number')]]";
    String POKEMON_TYPE_TITLE_SPAN = "//a[@title='Type' and ancestor::b]//span";
    String POKEMON_FORM_TYPE_TEXTS = "//table[preceding-sibling::b/a[@title='Type']]//b[ancestor::a[not(contains(@class, 'redirect'))]]";
}