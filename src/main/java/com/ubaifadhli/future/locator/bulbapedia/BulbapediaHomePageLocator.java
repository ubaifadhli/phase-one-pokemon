package com.ubaifadhli.future.locator.bulbapedia;

public interface BulbapediaHomePageLocator {
    String SEARCH_INPUT = "//input[@id='searchInput' and contains(@class, 'global')]";
    String WELCOME_TEXT = "//th[contains(text(), 'Welcome to Bulbapedia!')]";
}
