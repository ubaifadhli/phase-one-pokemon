package com.ubaifadhli.future.locator.pokedex;

import org.openqa.selenium.By;

public interface PokedexLocator {
    By POPUP_CONTINUE_BUTTON = By.id("dev.ronnie.pokeapiandroidtask:id/confirm_button");
    By POKEMON_SEARCH_FIELD= By.id("dev.ronnie.pokeapiandroidtask:id/searchView");
    By POKEMON_SEARCH_RESULT_TEXTS = By.xpath("//android.widget.TextView[@resource-id='dev.ronnie.pokeapiandroidtask:id/pokemon_item_title']");
    By POKEMON_NUMBER_TEXT = By.id("dev.ronnie.pokeapiandroidtask:id/pokemon_number");
    String POKEMON_STAT_NUMBER_TEXT_BY_NAME = "//android.widget.TextView[@resource-id='dev.ronnie.pokeapiandroidtask:id/stat_count' and following-sibling::android.widget.TextView[@text='%s']]";
    By POKEMON_NAME_TEXT = By.xpath("//android.view.ViewGroup[@resource-id='dev.ronnie.pokeapiandroidtask:id/toolbar']//android.widget.TextView");
}
