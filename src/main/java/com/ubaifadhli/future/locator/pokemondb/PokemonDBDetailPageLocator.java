package com.ubaifadhli.future.locator.pokemondb;

public interface PokemonDBDetailPageLocator {
    String POKEMON_NUMBER_STRONG = "(//h2[text()='Pokédex data']//following::table)[1]//strong";
    String POKEMON_TYPES_LINKS = "(//h2[text()='Pokédex data']//following::table)[1]//a[contains(@class, 'type-icon')]";
    String POKEMON_STAT_VALUES = "(//h2[text()='Base stats']//following::table)[1]//tr//td[1]";
}
