package com.ubaifadhli.future.locator.pokemondb;

public interface PokemonDBSearchPageLocator {
    String NO_RESULTS_BOX = "//div[@class='gs-snippet' and text()='No Results']";
    String SEARCH_RESULT_LINKS = "//div[@class='gsc-thumbnail-inside']//a";
    String SEARCH_RESULT_LINKS_BOLD_TEXT = SEARCH_RESULT_LINKS + "/" + "b";
}
