package com.ubaifadhli.future.locator.pokemondb;

public interface PokemonDBHomePageLocator {
    String SEARCH_INPUT = "//input[@id='sitesearch']";
    String MODAL_CLOSE_SPAN = "//div[contains(@class, 'modal-wrapper visible')]//span";
    String WELCOME_TEXT = "//div[contains(@class, 'panel-intro')]//b[contains(text(), 'Welcome to')]";
}
