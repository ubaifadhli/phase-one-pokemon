package com.ubaifadhli.future.page.bulbapedia;

import com.ubaifadhli.future.page.PageObject;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import static com.ubaifadhli.future.locator.bulbapedia.BulbapediaHomePageLocator.SEARCH_INPUT;
import static com.ubaifadhli.future.locator.bulbapedia.BulbapediaHomePageLocator.WELCOME_TEXT;

public class BulbapediaHomePage extends PageObject {

    public BulbapediaHomePage(WebDriver driver) {
        super(driver);
    }

    public void typeAndClickSearchInput(String input) {
        typeInput(SEARCH_INPUT, input, Keys.ENTER);
    }

    public void waitForWelcomeTextToStale() {
        waitForElementToStale(WELCOME_TEXT);
    }
}
