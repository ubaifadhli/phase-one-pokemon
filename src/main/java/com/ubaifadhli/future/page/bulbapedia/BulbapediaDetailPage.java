package com.ubaifadhli.future.page.bulbapedia;

import com.ubaifadhli.future.page.PageObject;
import com.ubaifadhli.future.util.StringFormattingHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.ubaifadhli.future.locator.bulbapedia.BulbapediaDetailPageLocator.*;

public class BulbapediaDetailPage extends PageObject {

    public BulbapediaDetailPage(WebDriver driver) {
        super(driver);
    }

    public int getPokemonNumber() {
        String pokemonNumberString = getTextFromClickableElement(POKEMON_NUMBER_SPAN);

        pokemonNumberString = pokemonNumberString.replace("#", "");

        return Integer.parseInt(pokemonNumberString);
    }

    private boolean pokemonHasMultipleFormTypes() {
        return getTextFromClickableElement(POKEMON_TYPE_TITLE_SPAN).equals("Types");
    }

    public List<String> getPokemonType(String pokemonName) {
        List<String> typeLists = new ArrayList<>();

        String formattedPokemonName = StringFormattingHelper.uppercaseFirstCharacter(pokemonName);

        String currentPath = pokemonHasMultipleFormTypes() ? String.format(POKEMON_FIRST_FORM_TYPE_TEXTS, formattedPokemonName) : POKEMON_FORM_TYPE_TEXTS;

        for (WebElement element : getElements(currentPath))
            typeLists.add(getTextFromClickableElement(element).toLowerCase());

        return typeLists;
    }

    public List<Integer> getPokemonBaseStats() {
        List<WebElement> statTables = getElements(POKEMON_STAT_TABLES);
        List<String> statTableSpans = getTextFromElements(POKEMON_STAT_TABLE_SPANS);

        // XPath index starts from 1.
        int tableIndex = 1;

        // If size of the table and span doesn't match, then it's highly probable that the first
        // index is the correct stat.
        if (statTables.size() == statTableSpans.size()) {

            // Searching for table headings that contain "Generation" string,
            // if exists then get the last index from the list.
            List<Integer> generationSpanIndexes = IntStream.range(0, statTableSpans.size())
                    .filter(i -> statTableSpans.get(i).contains("Generation"))
                    .boxed()
                    .collect(Collectors.toList());

            if (generationSpanIndexes.size() > 0)
                tableIndex = generationSpanIndexes.size();

        }

        List<WebElement> statNumbers = getElements(String.format(POKEMON_STAT_VALUES_BY_TABLE_INDEX, tableIndex));

        return statNumbers.stream()
                .map(WebElement::getText)
                .map(Integer::parseInt)
                .collect(Collectors.toList());
    }
}
