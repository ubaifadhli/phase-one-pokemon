package com.ubaifadhli.future.page.pokedex;

import com.ubaifadhli.future.constant.PokedexStatNameConstant;
import com.ubaifadhli.future.page.PageObject;
import com.ubaifadhli.future.util.MobileUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import static com.ubaifadhli.future.locator.pokedex.PokedexLocator.*;

public class PokedexPage extends PageObject {
    public PokedexPage(WebDriver driver) {
        super(driver);
    }

    public void clickPopupContinueButton() {
        clickElementAfterClickable(POPUP_CONTINUE_BUTTON);
    }

    public void clickAndTypeSearchView(String pokemonName) {
        WebElement searchField = getElement(POKEMON_SEARCH_FIELD);

        searchField.click();
        searchField.sendKeys(pokemonName);
    }

    public void scrollUp() {
        MobileUtil.scrollUp(driver, 75);
    }

    public By getPokemonStatNumberByName(PokedexStatNameConstant nameConstant) {
        return By.xpath(String.format(POKEMON_STAT_NUMBER_TEXT_BY_NAME, nameConstant.text));
    }

    public String getPokemonName() {
        return getTextFromClickableElement(POKEMON_NAME_TEXT);
    }

    public int getPokemonNumber() {
        return Integer.parseInt(getTextFromClickableElement(POKEMON_NUMBER_TEXT));
    }

    public int getSearchedPokemonIndex(String pokemonName) {
        List<WebElement> pokemonSearchResultCards = getPokemonSearchResult();

        if (pokemonSearchResultCards.size() == 0)
            return -1;

        for (int i = 0; i < pokemonSearchResultCards.size(); i++)
            if (pokemonSearchResultCards.get(i).getText().equalsIgnoreCase(pokemonName))
                return i;

        return -1;
    }

    public List<WebElement> getPokemonSearchResult() {
        return getElementsAfterClickable(POKEMON_SEARCH_RESULT_TEXTS);
    }

    public void clickPokemonSearchResultByIndex(int resultIndex) {
        getPokemonSearchResult().get(resultIndex).click();
    }

    public void clickEnterOnSearchView() {
        MobileUtil.pressEnter(driver);
    }
}
