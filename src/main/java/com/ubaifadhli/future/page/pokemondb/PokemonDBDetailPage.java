package com.ubaifadhli.future.page.pokemondb;

import com.ubaifadhli.future.page.PageObject;
import org.openqa.selenium.WebDriver;

import java.util.List;
import java.util.stream.Collectors;

import static com.ubaifadhli.future.locator.pokemondb.PokemonDBDetailPageLocator.*;

public class PokemonDBDetailPage extends PageObject {
    public PokemonDBDetailPage(WebDriver driver) {
        super(driver);
    }

    public List<String> getPokemonTypes() {
        List<String> pokemonTypes = getTextFromClickableElements(POKEMON_TYPES_LINKS);

        return pokemonTypes.stream().map(String::toLowerCase).collect(Collectors.toList());
    }

    public int getPokemonNumber() {
        return Integer.parseInt(getTextFromClickableElement(POKEMON_NUMBER_STRONG));
    }

    public List<Integer> getPokemonBaseStat() {
        List<String> values = getTextFromClickableElements(POKEMON_STAT_VALUES);

        return values.stream().map(Integer::parseInt).collect(Collectors.toList());
    }
}
