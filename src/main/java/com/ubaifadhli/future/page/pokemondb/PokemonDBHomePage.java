package com.ubaifadhli.future.page.pokemondb;

import com.ubaifadhli.future.page.PageObject;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import static com.ubaifadhli.future.locator.pokemondb.PokemonDBHomePageLocator.*;


public class PokemonDBHomePage extends PageObject {
    public PokemonDBHomePage(WebDriver driver) {
        super(driver);
    }

    public void typeSearchInput(String input, Keys inputKeys) {
        typeInput(SEARCH_INPUT, input, inputKeys);
    }

    public boolean isPrivacyModalVisible() {
        return doesElementExist(MODAL_CLOSE_SPAN);
    }

    public void clickCloseModalButton() {
        clickElementAfterClickable(MODAL_CLOSE_SPAN);
    }

    public void waitForWelcomeTextToStale() {
        waitForElementToStale(WELCOME_TEXT);
    }
}
