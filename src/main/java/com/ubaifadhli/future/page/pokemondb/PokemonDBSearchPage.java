package com.ubaifadhli.future.page.pokemondb;

import com.ubaifadhli.future.page.PageObject;
import org.openqa.selenium.WebDriver;

import static com.ubaifadhli.future.locator.pokemondb.PokemonDBHomePageLocator.MODAL_CLOSE_SPAN;
import static com.ubaifadhli.future.locator.pokemondb.PokemonDBSearchPageLocator.*;

public class PokemonDBSearchPage extends PageObject {

    public PokemonDBSearchPage(WebDriver driver) {
        super(driver);
    }

    public boolean doesNoResultBoxExists() {
        return doesElementExist(NO_RESULTS_BOX);
    }

    public String getFirstResultBoldText() {
        return getTextFromClickableElements(SEARCH_RESULT_LINKS_BOLD_TEXT).get(0);
    }

    public void clickCloseModalButton() {
        clickElementAfterClickable(MODAL_CLOSE_SPAN);
    }

    public boolean isPrivacyModalVisible() {
        return doesElementExist(MODAL_CLOSE_SPAN);
    }

    public void clickFirstResultLink() {
        clickElementAt(SEARCH_RESULT_LINKS, 0);
    }
}
