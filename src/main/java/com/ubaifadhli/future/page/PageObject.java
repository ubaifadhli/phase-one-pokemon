package com.ubaifadhli.future.page;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class PageObject {
    protected WebDriver driver;

    public PageObject(WebDriver driver) {
        this.driver = driver;
    }

    public void openPageAt(String url) {
        try {
            driver.get(url);
        } catch (TimeoutException timeoutException) {
            log.error("Page took too long to load. Trying to continue executing the test steps.");
        }
    }

    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    public WebElement getElementAfterClickable(String element) {
        return getElementAfterClickable(By.xpath(element));
    }

    public WebElement getElementAfterClickable(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
        return getElement(locator);
    }

    public List<WebElement> getElementsAfterClickable(String xpath) {
        return getElementsAfterClickable(By.xpath(xpath));
    }

    public List<WebElement> getElementsAfterClickable(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
        return getElements(locator);
    }

    public WebElement getElement(String element) {
        return getElement(By.xpath(element));
    }

    public WebElement getElement(By locator) {
        return driver.findElement(locator);
    }

    public boolean doesElementExist(String xpathLocator) {
        return doesElementExist(By.xpath(xpathLocator));
    }

    public boolean doesElementExist(By locator) {
        try {
            driver.findElement(locator);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public List<WebElement> getElements(String elements) {
        return driver.findElements(By.xpath(elements));
    }

    public List<WebElement> getElements(By locator) {
        return driver.findElements(locator);
    }

    public void clickElementAfterClickable(String element) {
        clickElementAfterClickable(By.xpath(element));
    }

    public void clickElementAfterClickable(By locator) {
        getElementAfterClickable(locator).click();
    }

    public void clickElementAt(String elements, int index) {
        getElements(elements).get(index).click();
    }

    public void typeInput(String element, String input, Keys keysInput) {
        typeInput(By.xpath(element), input, keysInput);
    }

    public void typeInput(By locator, String input, Keys keysInput) {
        WebElement foundElement = getElement(locator);

        foundElement.clear();

        foundElement.sendKeys(input, keysInput);
    }

    public String getTextFromClickableElement(String element) {
        return getElementAfterClickable(element).getText();
    }

    public String getTextFromClickableElement(By locator) {
        return getElementAfterClickable(locator).getText();
    }

    public List<String> getTextFromElements(String elements) {
        return getElements(elements).stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    public List<String> getTextFromClickableElements(String elements) {
        List<String> texts = new ArrayList<>();

        for (WebElement element : getElementsAfterClickable(elements))
            texts.add(element.getText());

        return texts;
    }

    public String getTextFromClickableElement(WebElement element) {
        return element.getText();
    }

    public void waitForElementToStale(String element) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 5);
            wait.until(ExpectedConditions.stalenessOf(getElement(element)));
        } catch (NoSuchElementException exception) {
            log.info("Staling element was not found. Trying to continue executing the test steps.");
        }
    }
}
