package com.ubaifadhli.future.data.pokeapi.response;

import com.ubaifadhli.future.data.PokemonData;
import lombok.ToString;

import java.util.List;
import java.util.stream.Collectors;

@ToString
public class PokeAPIResponse {
    private int id;
    private String name;
    private List<PokeAPIStatsResponse> stats;
    private List<PokeAPITypesResponse> types;

    public PokemonData toPokemonData() {
        List<String> typeList = types.stream()
                .map(type -> type.getType().getName())
                .collect(Collectors.toList());

        List<Integer> statList = stats.stream()
                .map(PokeAPIStatsResponse::getBase_stat)
                .collect(Collectors.toList());

        int statTotal = statList.stream()
                .reduce(0, Integer::sum);

        statList.add(statTotal);

        PokemonData pokemonData = PokemonData.builder()
                .number(id)
                .name(name)
                .types(typeList)
                .build();

        pokemonData.setStatData(statList);

        return pokemonData;
    }
}
