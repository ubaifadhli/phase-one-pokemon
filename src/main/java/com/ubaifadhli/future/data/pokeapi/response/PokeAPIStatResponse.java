package com.ubaifadhli.future.data.pokeapi.response;

import lombok.ToString;

@ToString
public class PokeAPIStatResponse {
    private String name;
}
