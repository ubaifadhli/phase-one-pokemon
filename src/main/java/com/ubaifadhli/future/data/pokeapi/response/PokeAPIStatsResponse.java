package com.ubaifadhli.future.data.pokeapi.response;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class PokeAPIStatsResponse {
    private int base_stat;
    private PokeAPIStatResponse stat;
}
