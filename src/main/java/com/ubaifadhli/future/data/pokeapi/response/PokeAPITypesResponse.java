package com.ubaifadhli.future.data.pokeapi.response;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class PokeAPITypesResponse {
    private PokeAPITypeResponse type;
}
