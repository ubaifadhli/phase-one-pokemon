package com.ubaifadhli.future.data;

import io.restassured.response.Response;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class APIResult {
    private Response response;
    private String requestLog;
}
