package com.ubaifadhli.future.data;

import com.github.ubaifadhli.annotations.CSVColumn;
import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor              // CSV Processor would need no args constructor to instantiate the class.
@AllArgsConstructor             // Fix issue when @Builder and @NoArgsConstructor are used together.
public class PokemonData {
    private String name;
    private int number;
    @CSVColumn(splitByCharacter = '|')
    private List<String> types;
    private int hp;
    private int attack;
    private int defense;
    private int specialAttack;
    private int specialDefense;
    private int speed;
    private int total;

    public void setStatData(List<Integer> statLists) {
        // I still don't have better idea to get rid of this.
        hp = statLists.get(0);
        attack = statLists.get(1);
        defense = statLists.get(2);
        specialAttack = statLists.get(3);
        specialDefense = statLists.get(4);
        speed = statLists.get(5);
        total = statLists.get(6);
    }
}
