package com.ubaifadhli.future.constant;

public enum PokedexStatNameConstant {
    HP("Hp"),
    ATTACK("Attack"),
    DEFENSE("Defense"),
    SPECIAL_ATTACK("Special - Attack"),
    SPECIAL_DEFENSE("Special - Defense"),
    SPEED("Speed");

    public final String text;

    PokedexStatNameConstant(String text) {
        this.text = text;
    }
}
