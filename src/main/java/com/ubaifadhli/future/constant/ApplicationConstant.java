package com.ubaifadhli.future.constant;

public enum ApplicationConstant {
    Bulbapedia("https://bulbapedia.bulbagarden.net/wiki/"),
    PokemonDB("https://pokemondb.net/"),
    PokeAPI("https://pokeapi.co/api/v2/pokemon/"),
    Pokedex("");

    public String url;

    ApplicationConstant(String url) {
        this.url = url;
    }
}
