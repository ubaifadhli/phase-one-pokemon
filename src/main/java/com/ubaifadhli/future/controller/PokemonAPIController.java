package com.ubaifadhli.future.controller;

import com.ubaifadhli.future.data.APIResult;

public interface PokemonAPIController {
    APIResult getPokemonData(String pokemonName);
}
