package com.ubaifadhli.future.controller;

import com.ubaifadhli.future.data.APIResult;
import com.ubaifadhli.future.constant.ApplicationConstant;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

import static io.restassured.RestAssured.given;

public class PokeAPIController implements PokemonAPIController {
    public APIResult getPokemonData(String pokemonName) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);

        Response response = given()
                .filter(RequestLoggingFilter.logRequestTo(printStream))
                .accept(ContentType.JSON)
                .when()
                .get(ApplicationConstant.PokeAPI.url + pokemonName)
                .then()
                .extract()
                .response();

        String requestLog;

        try {
            requestLog = outputStream.toString("UTF8");
        } catch (UnsupportedEncodingException e) {
            requestLog = "Failed to obtain request header.";
            e.printStackTrace();
        }

        requestLog = requestLog.split("Proxy")[0];

        return APIResult.builder()
                .response(response)
                .requestLog(requestLog)
                .build();
    }
}