package com.ubaifadhli.future.exception;

public class PokemonNotFoundException extends RuntimeException {
    private static final String message = "Pokemon with specified name was not found : ";

    public PokemonNotFoundException(String pokemonName) {
        super(message + pokemonName);
    }
}
