package com.ubaifadhli.future.exception;

import com.ubaifadhli.future.constant.ApplicationConstant;

public class UnexpectedPokemonException extends RuntimeException{
    private static final String message = "Data for %s is %s to be found in %s.";

    public UnexpectedPokemonException(String pokemonName, boolean isPokemonFound, ApplicationConstant applicationConstant) {
        super(String.format(
                message,
                pokemonName, (isPokemonFound ? "not expected" : "expected"), applicationConstant.name())
        );
    }
}
